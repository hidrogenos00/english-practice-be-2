import express, { Express } from "express";
import {authRouter} from "./modules/auth/auth.router";
import {userRouter} from "./modules/user/user.router";

export class RouterApi {
    static init(app: Express) {
        const router = express.Router();
        app.use('/api/v1', router);
        router.use('/auth', authRouter);
        router.use('/user', userRouter);
    }
}