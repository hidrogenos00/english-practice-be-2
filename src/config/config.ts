import dotenv from 'dotenv';

dotenv.config();

export interface IConfig {
    env: string | undefined;
    port: string | number | undefined;
    dbUser: string | undefined;
    dbPassword: string | undefined;
    dbHost: string | undefined;
    dbName: string | undefined;
    dbPort: string | undefined;
    auth_secret: string | undefined;
}

export const config: IConfig = {
    env: process.env.NODE_ENV || 'dev',
    port: process.env.PORT || 3000,
    dbUser: process.env.DB_USER,
    dbPassword: process.env.DB_PASSWORD,
    dbHost: process.env.DB_HOST,
    dbName: process.env.DB_NAME,
    dbPort: process.env.DB_PORT,
    auth_secret: process.env.AUTH_SECRET || 'auth_secret',
}