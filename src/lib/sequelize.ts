import { Sequelize } from "sequelize";
import { getDbConnectionUrl } from "../utils/helpers";
import { setupModels } from "../db/models";

const URI = getDbConnectionUrl();

export const sequelize = new Sequelize(URI, {
    dialect: 'postgres',
    logging: false
});

setupModels(sequelize);
