import {ValidationError} from "sequelize";
import {StatusCodes} from "http-status-codes";
import {NextFunction, Request, Response} from "express";
import {BaseError} from "../utils/base-error";

export function OrmErrorHandler(error: any, req: Request, res: Response, next: NextFunction) {
    console.log('HAR-ormErrorHandler');
    if (error instanceof ValidationError) {
        res.status(StatusCodes.CONFLICT).json({
            statusCode: StatusCodes.CONFLICT,
            message: error.name,
        });
    } else {
        next(error);
    }
}

export function BaseErrorHandler(error: any, req: Request, res: Response, next: NextFunction) {
    console.log('HAR-baseErrorHandler');
    if (error instanceof BaseError) {
        res.status(error.statusCode).json({
            statusCode: error.statusCode,
            message: error.message,
        });
    } else {
        next(error);
    }
}

export function ErrorHandler(error: any, req: Request, res: Response, next: NextFunction) {
    console.log('HAR-unhadledErrorHandler');
    res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
        message: error.message,
    });
    next(error);
}