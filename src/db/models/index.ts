import {UserModel, userSchema} from "./user.model";
import {Sequelize} from "sequelize";

export function setupModels(sequelize: Sequelize): void {
    UserModel.init(userSchema, { tableName: 'Users', sequelize})
}