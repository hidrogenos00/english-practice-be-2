import {Model, InferAttributes, CreationOptional, InferCreationAttributes, DataTypes} from 'sequelize';

export class UserModel extends Model<InferAttributes<UserModel>, InferCreationAttributes<UserModel>> {
    declare id: CreationOptional<number>;
    declare name: string;
    declare email: string;
    declare password: string;
    declare avatar: string | null;
    declare createdAt: CreationOptional<Date>;
    declare updatedAt: CreationOptional<Date>;
}

export const userSchema = {
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    name: {
        type: new DataTypes.STRING(128),
        allowNull: false,
        unique: true,
    },
    email: {
        type: new DataTypes.STRING(128),
        allowNull: false,
        unique: true,
    },
    password: {
        type: new DataTypes.STRING(128),
        allowNull: false,
    },
    avatar: {
        type: new DataTypes.STRING(128),
        allowNull: true
    },
    createdAt: {
        type: new DataTypes.DATE(),
        allowNull: false
    },
    updatedAt: {
        type: new DataTypes.DATE(128),
        allowNull: false
    }
};