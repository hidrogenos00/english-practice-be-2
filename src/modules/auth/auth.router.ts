import express, { Request, Response } from "express";

export const authRouter = express.Router();

authRouter.get(
    '/login',
    async (req: Request, res: Response, next) => {
        try {
            res.json({message: 'success'});
        } catch (e) {
            next(e);
        }
    }
)

