import {UserModel} from "../../db/models/user.model";

export class UserService {
    async getUsers(): Promise<any> {
        return await UserModel.findAll();
    }

    async createUser(createUserDto: any): Promise<any> {
        return await UserModel.create(createUserDto);
    }
}
