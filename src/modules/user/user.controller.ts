import {UserService} from "./user.service";
import {NextFunction, Request, Response} from "express";

const userService = new UserService();

export class UserController {
    async getAllUsers(req: Request, res: Response) {
        const result = await userService.getUsers();
        res.json(result);
    }

    async createUser(req: Request, res: Response, next: NextFunction): Promise<any> {
        const newUser = req.body;
        try {
            const result = await userService.createUser(newUser);
            res.json(result);
        } catch (e) {
            next(e);
        }
    }
}