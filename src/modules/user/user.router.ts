import express, { Request, Response } from "express";
import {UserController} from "./user.controller";

export const userRouter = express.Router();

const userController = new UserController();
userRouter.get(
    '/',
    userController.getAllUsers
)

userRouter.post(
    '/',
    userController.createUser
)
