import express, {Express, Request, Response} from "express";
import {sequelize} from "./src/lib/sequelize";
import {RouterApi} from "./src/roterApi";
import expressWinston from 'express-winston';
import winston from 'winston';
import {BaseErrorHandler, ErrorHandler, OrmErrorHandler} from "./src/middlewares/error.handler";
import {BaseError} from './src/utils/base-error';

const app: Express = express();
const port = process.env.PORT;
app.use(express.json());



//  logger
// app.use(expressWinston.logger({
//     transports: [
//         new winston.transports.Console()
//     ],
//     format: winston.format.combine(
//         winston.format.colorize(),
//         winston.format.json()
//     ),
//     meta: true,
//     msg: "HTTP {{req.method}} {{req.url}}",
//     expressFormat: true,
//     colorize: false,
//     // ignoreRoute: function (req, res) { return false; }
// }));

//  Routes

RouterApi.init(app);





app.get('/', async (req: Request, res: Response) => {
    try {
        console.log(req.query);
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');

        res.json({message: 'ok'});
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }

});

app.get('/test-error', () => {
    throw new Error('some error');
});

app.get('/test-base-error', () => {
    throw new BaseError('some base error', 401, 'some abse error');
});

//  Error logger

app.use(expressWinston.errorLogger({
    transports: [
        // new winston.transports.Console(),
        new winston.transports.File({ filename: 'error.log' }),
    ],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
    )
}));

app.use(BaseErrorHandler);
app.use(OrmErrorHandler);
app.use(ErrorHandler);


app.listen(port, () => {
    console.log(`[server]: Server is running at http://localhost:${port}`);
});
